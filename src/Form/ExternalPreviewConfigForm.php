<?php

namespace Drupal\external_preview\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * This class renders and manages the configuration form of the module.
 *
 * @package Drupal\external_preview\Form
 */
class ExternalPreviewConfigForm extends ConfigFormBase {

  const FORM_ID = 'external_preview_config_form';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * Builds and returns the configuration form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('external_preview.settings');

    $previewUrl = $config->get('external_preview.preview_url');

    $form['preview_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('External Preview Url'),
      '#default_value' => $previewUrl,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#description' => $this->t("
        <ul>
          <li>When the preview button is clicked it will POST editor form data to the specified url</li>
          <li>eg. http://www.test.com:80</li>
        </ul>"),
      '#required' => FALSE,
    );

    return $form;
  }

  /**
   * Validates the users's input.
   *
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $formNames = $form_state->getValue('preview_url');
    /*
    foreach ($formNames as &$name) {
      $name = trim($name);
      preg_match('/[^\w]+/', $name, $matches);
      if (count($matches)) {
        if (@preg_match($name, NULL) === FALSE) {
          $form_state->setErrorByName('form_names',
            $this->t('Form name "%name" contains non wordy characters and is 
            not a regexp.',
              ['%name' => $name]
            )
          );
        }
      }
    }
    */

    parent::validateForm($form, $form_state);
  }

  /**
   * Handles the post validation process.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $previewUrl = $form_state->getValue('preview_url');
    //$previewUrl = $this->multiline2Array($previewUrl);

    $config = $this->config('external_preview.settings');
    $config->set('external_preview.preview_url', $previewUrl);

    $config->save();

    parent::submitForm($form, $form_state);
  }


  /**
   * Returns the editable config names.
   *
   * @return array
   *   Returns an array of the editable config names.
   */
  protected function getEditableConfigNames() {
    return [
      'external_preview.settings',
    ];
  }

  /**
   * Filters empty and null values but not 0.
   *
   * @param mixed $value
   *   Array value to be filtered.
   *
   * @return bool
   *   Returns FALSE if the value must be filtered.
   */
  private function emptyStringFilter($value) {
    return ($value !== NULL && $value !== '');
  }
}
