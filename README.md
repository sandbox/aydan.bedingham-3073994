# Drupal 8 - External Preview

A Drupal8 module to alter the functionality of the preview button to open an external url.

The editor form data is posted to the specified url.

## Installation

Place the module in your `modules` folder and enable it as usual with drush 
`drush en external_preview` or from the `/admin/modules` page.

## Configuration

Go to the config page of the module `/admin/config/external_preview` and fill in 
the external url to be opened when the preview button is clicked.